const words = [
    { word: "hello", translation: "hola" },
    { word: "goodbye", translation: "adios" },
    { word: "please", translation: "por favor" },
    { word: "thank you", translation: "gracias" },
    { word: "yes", translation: "si" },
    { word: "no", translation: "no" },
    { word: "apple", translation: "manzana" },
    { word: "banana", translation: "platano" },
    { word: "orange", translation: "naranja" },
    { word: "watermelon", translation: "sandia" },
    { word: "cat", translation: "gato" },
    { word: "dog", translation: "perro" },
  ];
  
  const cards = [];
  
  let selectedCards = [];
  
  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }
  
  function createCard(word) {
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerText = word;
    card.addEventListener("click", () => {
      if (selectedCards.length < 2 && !selectedCards.includes(card)) {
        card.classList.add("matched");
        selectedCards.push(card);
        if (selectedCards.length === 2) {
          const [card1, card2] = selectedCards;
          if (card1.innerText === card2.innerText) {
            card1.removeEventListener("click", () => {});
            card2.removeEventListener("click", () => {});
            selectedCards = [];
          } else {
            setTimeout(() => {
              card1.classList.remove("matched");
              card2.classList.remove("matched");
              selectedCards = [];
            }, 1000);
          }
        }
      }
    });
    return card;
  }
  
  function start() {
    const container = document.querySelector(".container");
    const shuffledWords = shuffle(words.concat(words));
    shuffledWords.forEach((word) => {
      const card = createCard(word.word);
      cards.push(card);
      container.appendChild(card);
    });
  }
  
  start();
  