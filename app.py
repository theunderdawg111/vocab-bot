"""app.py"""
import telegram
from telegram import Update, KeyboardButton, ReplyKeyboardMarkup, WebAppInfo , InlineKeyboardButton, InlineKeyboardMarkup  
from telegram.ext import ApplicationBuilder, CallbackContext, CommandHandler, MessageHandler, filters, CallbackQueryHandler, Updater
from credentials import BOT_TOKEN, BOT_USERNAME, WEBAPP_URL
import json


async def launch_web_ui(update: Update, callback: CallbackContext):
    # display our web-app!
    kb = [
        [KeyboardButton(
            "Show me my Web-App!", 
           web_app=WebAppInfo("https://theunderdawg111.gitlab.io/vocab-bot/")
           web_app=WebAppInfo(WEBAPP_URL) # obviously, set yours here.
        )]
    ]
    await update.message.reply_text("Let's do this...", reply_markup=ReplyKeyboardMarkup(kb))

def start(update, context):
    # Create an inline keyboard with a "Go to Chat" button
    chat_id = update.message.chat_id
    keyboard = [[InlineKeyboardButton("Go to Chat", url="https://t.me/mukundc_bot")]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    context.bot.send_message(chat_id=chat_id, text="Click the button to go to the chat!", reply_markup=reply_markup)

bot = telegram.Bot(token=BOT_TOKEN)


if __name__ == '__main__':
    # when we run the script we want to first create the bot from the token:
    application = ApplicationBuilder().token(BOT_TOKEN).build()

    # and let's set a command listener for /start to trigger our Web UI
    application.add_handler(CommandHandler('play', launch_web_ui))
    application.add_handler(CommandHandler('start',start))

    # and send the bot on its way!
    print(f"Your bot is listening! Navigate to http://t.me/{BOT_USERNAME} to interact with it!")
    application.run_polling()
    

